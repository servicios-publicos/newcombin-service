# Desafío Backend

En este desafío, creará una API REST de una versión súper simplificada de un proveedor de servicios de pago de impuestos. Tendrá funcionalidades para listar las boletas con filtros, crear boletas, crear los pagos de las boletas como también listar estos pagos con sus filtros respectivos.

## Tabla de Contenido 📋

- [Desafío Backend](#desafío-backend)
  - [Tabla de Contenido 📋](#tabla-de-contenido-)
  - [Dependencias](#dependencias)
  - [Pre-requisitos](#pre-requisitos)
  - [Variables de entorno](#variables-de-entorno)
      - [Service Config](#service-config)
      - [Config Database](#config-database)
  - [Quick Start](#quick-start)
  - [API](#api)

## Dependencias
A continuación, se presentarán las dependencias más importantes que se usaron en la construcción del servicio

| Módulo | Versión | Tamaño
| ------ | ------ | ------ |
| pg  | 8.7.3 | [![pg](https://packagephobia.com/badge?p=pg)](https://node-postgres.com) |
| express  | 4.17.2 | [![express](https://packagephobia.com/badge?p=express)](https://github.com/expressjs/express) |
| module-alias  | 2.2.2 | [![install size](https://packagephobia.com/badge?p=module-alias)](https://github.com/ilearnio/module-alias) |
| winston  | 3.3.3 | [![winston](https://packagephobia.com/badge?p=winston)](https://github.com/winstonjs/winston) |
| winston-daily-rotate-file  | 4.5.5 | [![winston-daily-rotate-file](https://packagephobia.com/badge?p=winston-daily-rotate-file@4.5.5)](https://packagephobia.com/winston-daily-rotate-file@4.5.5) |
| sequelize  | 4.5.5 | [![sequelize](https://packagephobia.com/badge?p=sequelize)](https://sequelize.org/docs/v6/getting-started/) |

## Pre-requisitos
Para poder ejecutar el servicio es necesario cumplir con los siguientes pre-requisitos:

1. Tener instalado la versión de node v16.15.1
2. Tener instalado una base de datos con Postgres
5. Tener instalado eslint de manera global. Para más información, [clic aquí.](https://eslint.org/docs/user-guide/getting-started).
6. Tener instalador nodemon de manera global. Para más información, [clic aquí.](https://www.npmjs.com/package/nodemon).
7. Clonar el servicio del siguiente repositorio, [clic aquí.](https://gitlab.com/servicios-publicos/newcombin-service)
8. Configurar correctamente las variables de entorno. Ver sección **Variables de entorno**


## Variables de entorno
Crear el archivo `.env` en la raíz del proyecto y agregar las siguientes variables de entorno. En el repositorio encontraras un archivo `.env-sample` con valores de ejemplos.

#### Service Config
```
NODE_ENV                = Entorno de desarrollo
NODE_PORT               = Puerto en el que el servicio será levantado
NODE_PREFIX             = Prefijo usado para las rutas, por ejemplo: /api
```
#### Config Database
```
DB_DIALECT                          = Valor que indica a qué tipo de base de datos se conectará el servicio. Para el test se ha usado 'postgres'. Más información en el siguiente link https://sequelize.org/docs/v6/getting-started/
DB_HOST                             = Host en donde se encuentre la base de datos
DB_PORT                             = Puerto de ejecución de la base de datos
DB_USER                             = Usuario de la base de datos
DB_PASSWORD                         = Contraseña del usuario
DB_DATABASE                         = Nombre de la base de datos
DB_SCHEMA                           = Esquema al que se tiene que conectar el servicio
```


## Quick Start

A continuación, se prosigue a explicar los pasos para levantar el servicio. Tomar en cuenta que ya se debe de tener clonado el servicio.


* Ubicarse en la carpeta raíz del servicio
* Asegúrese de haber configurado las variables de entorno
* Instalar los paquetes ejecutando el siguiente comando:
```bash
$ npm run install || yarn install
```
* Finalmente, levantar el servicio ejecutando el comando
```bash
$ npm run dev || yarn run dev
```

## API
La documentación de la API podrás encotrar en la siguiente ruta, [clic aquí](https://documenter.getpostman.com/view/5107766/UzJFuxvS).