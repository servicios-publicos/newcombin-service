'use strict'
require(`winston-daily-rotate-file`)
const { createLogger, format, transports, config: confWins } = require(`winston`)
const { combine, timestamp, label, printf } = format

// Environment variables
const config = require(`@/config`)
const { logs } = config

// Format response logger
const myFormat = printf(({ level, message, label: labelText, timestamp: timestampFormat }) => {
  return `${timestampFormat} [${labelText}] ${level}: ${message}`
})

class Winston {
  constructor (lbl) {
    this.logger = createLogger({
      level: logs.level,
      levels: confWins.npm.levels,
      format: combine(
        label({ label: lbl }),
        timestamp({ format: `YYYY-MM-DD HH:mm:ss` }),
        myFormat
      ),
      transports: [
        new transports.Console({
          colorize: true,
          name: `console`,
          prettyPrint: true
        }),
        new transports.DailyRotateFile({
          filename: `logs/audit-runtime-%DATE%.log`,
          datePattern: `YYYY-MM-DD`,
          maxSize: `20m`,
          maxFiles: `7d`
        })
      ]
    })
  }

  info (message) {
    this.logger.info(message)
  }
  
  error (message) {
    this.logger.error(message)
  }

  warning (message) {
    this.logger.warn(message)
  }
}

module.exports = lbl => new Winston(lbl)
