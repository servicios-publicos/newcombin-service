// Loaders
const expressLoader = require(`./express`)

// Init logger
const path = require(`path`)
const scriptName = path.basename(__filename)
const logger = require(`./logger`)(scriptName)

module.exports = ({ expressApp }) => {
  // Load express server
  expressLoader({ app: expressApp })
  logger.info(`✌️ Express loaded`)
}
