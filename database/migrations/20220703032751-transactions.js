'use strict'

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('transactions', { id: Sequelize.INTEGER });
     */

    await queryInterface.createTable(`transactions`, {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      paymentMethod: {
        type: Sequelize.ENUM([`debit_card`, `credit_card`, `cash`]),
        allowNull: false,
        comment: `Método de pago: debit_card, credit_card o cash`
      },
      cardNumber: {
        type: Sequelize.STRING,
        comment: `Número de tarjeta`
      },
      amount: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
        comment: `Importe`
      },
      barCode: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: `Número de boleta a la que pertenece`,
        references: {
          model: `payables`,
          key: `barCode`
        }
      },
      paymentDate: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        comment: `Fecha de pago`
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      schema: process.env.DB_SCHEMA,
      timestamps: true
    })
  },

  async down (queryInterface) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable(`transactions`)
  }
}
