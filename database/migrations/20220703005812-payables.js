'use strict'

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('payables', { barCode: Sequelize.STRING });
     */

    await queryInterface.createTable(`payables`, {
      barCode: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true,
        allowNull: false,
        comment: `Código de barras`
      },
      typeService: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: `Tipo de servicio: Luz, gas, etc`
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: `Descripción del servicio`
      },
      expirationDate: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        comment: `Fecha de vencimiento`
      },
      amount: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
        comment: `Importe a pagar`
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: `Estado en el que se encuentra la boleta`
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      schema: process.env.DB_SCHEMA,
      timestamps: true
    })
  },

  async down (queryInterface) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('payables');
     */

    await queryInterface.dropTable(`payables`)
  }
}
