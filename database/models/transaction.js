const { Sequelize } = require(`sequelize`)

// Loader database
const connection = require(`@loaders/database`)

// Custom array of validations
const typesPaymentMethods = [`debit_card`, `credit_card`, `cash`]
const validNumberCard = [`debit_card`, `credit_card`]

// Model dedinition
const Transaction = connection.define(`transactions`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  paymentMethod: {
    type: Sequelize.ENUM(typesPaymentMethods),
    allowNull: false,
    validate: {
      notEmpty: true,
      isIn (value) {
        const isInclude = typesPaymentMethods.includes(value)
        if (!isInclude) {
          throw new Error(`Enter one of these values: ${typesPaymentMethods}`)
        }
      }
    }
  },
  cardNumber: {
    type: Sequelize.STRING,
    validate: {
      notEmpty (value) {
        const isDiffToCash = validNumberCard.includes(this.paymentMethod)
        if (isDiffToCash) {
          if (value === ``) {
            throw new Error(`Card number is required`)
          }
        }
      }
    }
  },
  amount: {
    type: Sequelize.DECIMAL(10, 2),
    allowNull: false,
    validate: {notEmpty: true}
  },
  barCode: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {notEmpty: true}
  },
  paymentDate: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    validate: {notEmpty: true}
  },
  createdAt: {type: Sequelize.DATE},
  updatedAt: {type: Sequelize.DATE}
}, {schema: process.env.DB_SCHEMA})

module.exports = Transaction
