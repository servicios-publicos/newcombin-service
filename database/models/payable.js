const { Sequelize } = require(`sequelize`)

// Models references
const Transaction = require(`@database/models/Transaction`)

// Loader database
const connection = require(`@loaders/database`)

// Model dedinition
const Payable = connection.define(`payables`, {
  barCode: {
    type: Sequelize.STRING,
    primaryKey: true,
    unique: true,
    validate: {notEmpty: true}
  },
  typeService: {
    type: Sequelize.STRING,
    validate: {notEmpty: true}
  },
  description: {type: Sequelize.TEXT},
  expirationDate: {
    type: Sequelize.DATEONLY,
    validate: {notEmpty: true}
  },
  amount: {
    type: Sequelize.DECIMAL(10, 2),
    validate: {notEmpty: true}
  },
  status: {
    type: Sequelize.STRING,
    validate: {notEmpty: true}
  },
  createdAt: {type: Sequelize.DATE},
  updatedAt: {type: Sequelize.DATE}
}, {schema: process.env.DB_SCHEMA})

Payable.hasOne(Transaction, {
  foreignKey: `barCode`,
  targetKey: `barCode`
})

Transaction.belongsTo(Payable)

module.exports = Payable
