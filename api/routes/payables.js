const express = require(`express`)
const route = express.Router()

// PayablesController
const { index, save } = require(`@api/controllers/Http/PayablesController`)

module.exports = ({ app }) => {
  app.use(`/payables`, route)

  route.get(`/`, index())
  route.post(`/`, save())
}
