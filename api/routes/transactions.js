const express = require(`express`)
const route = express.Router()

// PayablesController
const { index, save } = require(`@api/controllers/Http/TransactionsController`)

module.exports = ({ app }) => {
  app.use(`/transactions`, route)

  route.get(`/`, index())
  route.post(`/`, save())
}
