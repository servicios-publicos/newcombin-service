"user strict"

const { Sequelize } = require(`sequelize`)

// Models
const Transaction = require(`@database/models/transaction`)
const Payable = require(`@database/models/payable`)

// Helpers
const ResponseHelper = require(`@helpers/Response`)

// Init logger
const path = require(`path`)
const scriptName = path.basename(__filename)
const logger = require(`@loaders/logger`)(scriptName)

exports.index = function() {
  return async (request, response) => {
    logger.info(`Get all transactions`)
    
    try {
      const Op = Sequelize.Op

      // Get query params
      const { query } = request
      const { startDate, endDate } = query

      // Add filter range date
      const whereQuery = {}
      if (startDate && endDate) {
        whereQuery.paymentDate = { [Op.gte]: startDate, [Op.lte]: endDate }
      } else if (startDate) {
        whereQuery.paymentDate = { [Op.gte]: startDate }
      } else if (endDate) {
        whereQuery.paymentDate = { [Op.lte]: endDate }
      }

      // Get all transactions
      const transactions = await Transaction.findAll({ raw: true, where: whereQuery })

      // Grouped transactions by paymentDate
      const groupedTransactions = transactions.reduce((counter, element) => {
        const key = element.paymentDate

        if (!counter[key]) counter[key] = []
        counter[key].push(element)
    
        return counter
      }, {})

      // Creating finally result
      const transactionsResult = []
      for (const [key, value] of Object.entries(groupedTransactions)) {
        const accumulatedAmount = value.reduce((total, element) => total + parseFloat(element.amount), 0)
        const result = { paymentDate: key, accumulatedAmount, transactionsByDate: value.length }
        transactionsResult.push(result)
      }

      // Return all transactions
      const formatSuccess = ResponseHelper.templateSuccess(transactionsResult)
      response.status(200).send(formatSuccess)
    } catch (error) {
      logger.error(error)
      const formatError = ResponseHelper.templateError(error.message)
      response.status(500).send(formatError)
    }
  }
}

exports.save = function() {
  return async (request, response) => {
    logger.info(`Save transaction into database`)
    
    try {
      // Get params body
      const { body } = request
      const { barCode, paymentMethod } = body
      let formatResponse = {}

      // Find payable
      const payable = await Payable.findOne({ where: { barCode } })
      if (payable === null) {
        const result = ResponseHelper.templateError(`Payable ${barCode} not found`)
        formatResponse = { result, statusCode: 404 }
      } else {
        // Update status to payable
        await payable.update({ status: `PAID` })

        // Delete card number if payment method is 'cash'
        if (paymentMethod === `cash`) delete body.cardNumber

        // Save transaction
        const transaction = await Transaction.create(body)
        const result = ResponseHelper.templateSuccess(transaction.toJSON())
        formatResponse = { result, statusCode: 200 }
      }

      // Return the transaction created
      response.status(formatResponse.statusCode).send(formatResponse.result)
    } catch (error) {
      console.error(error)
      const formatError = ResponseHelper.templateError(error.message)
      response.status(500).send(formatError)
    }
  }
}
