"user strict"

const { Sequelize } = require(`sequelize`)

// Models
const Payable = require(`@database/models/payable`)
const Transaction = require(`@database/models/transaction`)

// Helpers
const ResponseHelper = require(`@helpers/Response`)

// Init logger
const path = require(`path`)
const scriptName = path.basename(__filename)
const logger = require(`@loaders/logger`)(scriptName)

exports.index = function() {
  return async (request, response) => {
    logger.info(`Get all payables from database`)
    
    try {
      const Op = Sequelize.Op

      // Get query params
      const { query } = request
      const { typeService, status } = query

      // Add filter type service and status
      const whereQuery = {}
      if (typeService) whereQuery.typeService = { [Op.eq]: typeService }
      if (status) whereQuery.status = { [Op.like]: `%${status.toLowerCase()}%` }

      // Get all payables
      const payable = await Payable.findAll({ include: Transaction, where: whereQuery, raw: true })
      const payableResult = payable.map(element => {

        // Destructuring properties
        const { typeService: typeServiceData, expirationDate, amount, barCode, status, 'transaction.id': transactionId } = element
        let result = { typeService: typeServiceData, expirationDate, amount, barCode, status: status.toUpperCase() }

        // if the bill has already been paid
        if (transactionId) {
          const {
            'transaction.paymentMethod': paymentMethod,
            'transaction.cardNumber': cardNumber,
            'transaction.amount': amountTransaction,
            'transaction.barCode': barCodeTransaction,
            'transaction.paymentDate': paymentDate
          } = element
          const transaction = { paymentMethod, cardNumber, amount: amountTransaction, barCode: barCodeTransaction, paymentDate }
          result = { ...result, transaction }
        }

        // Eliminating property since it is being filtered by this data
        if (typeService) delete result.typeService

        return result
      })

      // Return all payables
      const formatSuccess = ResponseHelper.templateError(payableResult)
      response.status(200).send(formatSuccess)
    } catch (error) {
      logger.error(error)
      const formatError = ResponseHelper.templateError(error.message)
      response.status(500).send(formatError)
    }
  }
}

exports.save = function() {
  return async (request, response) => {
    logger.info(`Save payable into database`)
    
    try {
      // Get params body
      const { body } = request

      // Save payable
      const payable = await Payable.create({ ...body, status: body.status.toLowerCase() })

      // Return the payable created
      const formatSuccess = ResponseHelper.templateSuccess(payable.toJSON())
      response.status(200).send(formatSuccess)
    } catch (error) {
      logger.error(error)
      const formatError = ResponseHelper.templateError(error.message)
      response.status(500).send(formatError)
    }
  }
}
