const express = require(`express`)
const { Router } = express

// Routes
const payables = require(`./routes/payables`)
const transactions = require(`./routes/transactions`)

// Inject routes
module.exports = () => {
  const app = Router()

  // Routes for payables api
  payables({ app })

  // Routes for transactions api
  transactions({ app })

  return app
}
